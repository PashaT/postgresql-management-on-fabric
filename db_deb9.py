# -*-coding:utf-8-*-
from db_com import *
from db_com import __full_deploy


env.pg_version = '11'
env.postgis_version = '2.5'
env.os_codename = 'stretch'


@task
def full_deploy():
    __full_deploy()
