# -*-coding:utf-8-*-
from db_com import *

import re
from datetime import datetime


def backup_template(country_code):
    sudo('rm -f /tmp/txproject_{0}.dump'.format(country_code))
    pg_dump(db_name='txproject_{0}'.format(country_code), db_backup_file='/tmp/txproject_{0}.dump'.format(country_code))
    sudo('bzip2 /tmp/txproject_{0}.dump'.format(country_code), user='postgres')


def backup_all_templates():
    for country_code, url in db_templates.iteritems():
        backup_template(country_code)


def update_all_templates():
    for country_code, url in db_templates.iteritems():
        name = 'txproject_' + country_code
        sudo('psql -c "UPDATE pg_database SET datistemplate = FALSE WHERE datname = \'{}\';"'.format(name), user='postgres')
        sudo('dropdb {}'.format(name), user='postgres', warn_only=True)
        load_template('txproject_' + country_code, url)
        sudo('rm -f /tmp/txproject_{0}.dump'.format(country_code))


def restore_company_db(backup_file_name, domain):
    company_config = get_company_config(company_domain=domain)
    company_db_name = company_config['db']['name']
    company_db_user = company_config['db']['user']
    company_db_password = company_config['db']['password']

    restore_db(backup_file_name=backup_file_name, db_name=company_db_name, db_user=company_db_user, db_password=company_db_password)
    # migrate_company_db(domain=domain)


def migrate_company_db(domain):
    host = 'some.server'
    taxiclout_project_path = '/opt/taxicloud/'

    with cd(taxiclout_project_path):
        sudo('python manage.py txmigrate --domain={compay_domain} --procs=1'.format(compay_domain=domain))


def restore_db(backup_file_name, db_name, db_user=None, db_password=None):

    with cd('/tmp/'):
        if not db_user:
            db_user = db_name
        if not db_password:
            db_password = '!!!!!!!!!!!!!!!'

        if exists(backup_file_name) and '.dump.bz2' in backup_file_name:
            sudo('bzip2 -f -d {file}'.format(file=backup_file_name))
        backup_file_name = backup_file_name.replace('.bz2', '')

        create_role(user=db_user, password=db_password, attributes='LOGIN')
        create_db(db_name=db_name, owner=db_user)
        pg_restore(backup_file=backup_file_name, db_name=db_name)
        grant_db_previleges(db_name=db_name, user=db_user)


def backup_company_db(domain, zabbix_server_username):
    zabbix_server_hostname = 'some.server'
    if zabbix_server_username != 'root':
        zabbix_server_backup_dir = os.path.join('/home/', zabbix_server_username)
    else:
        zabbix_server_backup_dir = os.path.join('/{}/').format(zabbix_server_username)

    company_config = get_company_config(company_domain=domain)
    company_db_name = company_config['db']['name']
    pars_company_db_name = re.search(r'(?P<hash>.+)[_](.+)[_](.+)[_](.+)[_](.+)', company_db_name)
    dbname_hash = pars_company_db_name.group('hash')

    date = datetime.today().strftime('%Y-%m-%d_%H-%M')
    backup_file_name = '{hash}_{domain}_{date}'.format(hash=dbname_hash, domain=domain.replace('-', '_').replace('.', '_'), date=date)
    db_backup_file_arch = backup_db(db_name=company_db_name, backup_file_name=backup_file_name)

    sudo('scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null {file} {user}@{host}:{file_dir}'.format(file=db_backup_file_arch, user=zabbix_server_username, host=zabbix_server_hostname, file_dir=zabbix_server_backup_dir))


def backup_db(db_name, backup_file_name):
    db_backup_file = '/tmp/{backup_file_name}.dump'.format(backup_file_name=backup_file_name)
    db_backup_file_arch = db_backup_file + '.bz2'

    sudo('rm -f /tmp/{file}*'.format(file=backup_file_name))
    pg_dump(db_name=db_name, db_backup_file=db_backup_file)
    sudo('bzip2 {}'.format(db_backup_file))

    return db_backup_file_arch
