# -*-coding:utf-8-*-
import os
from fabric.api import *
from fabric.contrib.files import exists

from common import (update_whitelist, add_ssh_keys, get_hosts,
                    install_postgres, install_postgis, init_common_server_env, firewall_configurator,
                    install_wal_e, preparing_for_deploy, wget, get_company_config)


env.proj_dir = '/opt/'

env.postgis_used_versions = ['2.2', '2.5']

db_templates = {
    'az': 'https://link1',
    'by': 'https://link2',
    'kz': 'https://link3',
    'ru': 'https://link4',
    'ua': 'https://link5',
}


def grant_db_previleges(db_name, user):
    sudo('psql {db_name} -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO {db_user};"'.format(db_name=db_name, db_user=user), user='postgres')
    sudo('psql {db_name} -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO {db_user};"'.format(db_name=db_name, db_user=user), user='postgres')


def pg_restore(backup_file, db_name, options=''):
    postgis_preparing_db_restore()
    sudo('pg_restore {options} --no-owner --no-privileges -d {db_name} {backup_file}'.format(backup_file=backup_file, db_name=db_name, options=options), user='postgres')
    postgis_db_upgrade(db_name=db_name)


def pg_dump(db_name, db_backup_file):
    sudo('pg_dump -Fc {db_name} > {db_backup_file}'.format(db_name=db_name, db_backup_file=db_backup_file), user='postgres')


def create_txproject_db_user():
    create_role(user='txproject', password='!!!!!!!!!!!!', attributes='LOGIN CREATEDB CREATEROLE REPLICATION')


def create_role(user, password, attributes=''):
    sudo('psql -c "CREATE ROLE {user} WITH PASSWORD \'{password}\' {attributes};"'.format(user=user, password=password, attributes=attributes), user='postgres')


def create_db(db_name, owner):
    sudo('psql -c "CREATE DATABASE {db_name} WITH OWNER {db_owner} ENCODING = \'UNICODE\';"'.format(db_name=db_name, db_owner=owner), user='postgres')


def postgis_preparing_db_restore():
    if env.fabfile == 'db_man.py':
        return

    pg_version = getattr(env, 'pg_version')
    postgis_version = getattr(env, 'postgis_version')
    postgis_used_versions = getattr(env, 'postgis_used_versions')

    pg_lib_dir = '/usr/lib/postgresql/{pg_version}/lib'.format(pg_version=pg_version)

    with cd(pg_lib_dir):
        for postgis_migrate_from_version in postgis_used_versions:
            for postgis_so_files in ['postgis-VER.so', 'postgis_topology-VER.so', 'rtpostgis-VER.so']:
                symlink = postgis_so_files.replace('VER', postgis_migrate_from_version)
                destination = postgis_so_files.replace('VER', postgis_version)

                if postgis_migrate_from_version != postgis_version:
                    if exists(destination):
                        if not exists(symlink):
                            sudo('ln -s {destination} {symlink}'.format(destination=destination, symlink=symlink))
                        else:
                            warn('Can`t create symlink. PostGIS file {file} already exist.'.format(file=destination))
                    else:
                        raise EnvironmentError('PostGIS {version} executable file {file} does not exist.'.format(version=postgis_version, file=destination))


def postgis_db_upgrade(db_name):
    if env.fabfile == 'db_man.py':
        return

    postgis_version = getattr(env, 'postgis_version')
    pg_version = getattr(env, 'pg_version')

    postgis_script_dir = '/usr/share/postgresql/{pg_version}/contrib/postgis-{postgis_version}'.format(pg_version=pg_version, postgis_version=postgis_version)

    with cd(postgis_script_dir):
        using_extensions = sudo('echo "q" | psql {db_name} -c "SELECT PostGIS_Extensions_Upgrade();"'.format(db_name=db_name), user='postgres', warn_only=True)

        if using_extensions.return_code != 0:
            sudo('psql -f postgis_upgrade.sql -d {db_name}'.format(db_name=db_name), user='postgres')
            sudo('psql -f rtpostgis_upgrade.sql -d {db_name}'.format(db_name=db_name), user='postgres')
            sudo('psql -f topology_upgrade.sql -d {db_name}'.format(db_name=db_name), user='postgres')

    sudo('echo "q" | DEBIAN_FRONTEND=noninteractive psql {db_name} -c "SELECT postgis_full_version();"'.format(db_name=db_name), user='postgres')


def load_template(name=None, url=None):
    if not name:
        if not hasattr(env, 'template_name'):
            raise ValueError('template_name is not specified')
        name = env.template_name
    if not url:
        if hasattr(env, 'dump_url'):
            url = env.dump_url
        elif name in db_templates:
            url = db_templates[name]
        else:
            raise ValueError('dump_url is not specified')
    dump_path = '/tmp/{}.dump'.format(name)
    wget(output_file=dump_path + '.bz2', link=url)
    sudo('bzip2 -f -d {}.bz2'.format(dump_path))
    db_name = '{}'.format(os.path.basename(dump_path).replace('.dump', ''))
    create_db(db_name=db_name, owner='postgres')
    pg_restore(db_name=db_name, backup_file=dump_path)
    sudo('psql -c "UPDATE pg_database SET datistemplate = TRUE WHERE datname = \'{}\';"'.format(name), user='postgres')


def load_all_templates():
    for country_code, url in db_templates.iteritems():
        load_template('txproject_' + country_code, url)


def configure_firewall():
    firewall_configurator()


def wal_e_backup_push():
    with cd('/var/lib/postgresql/{}'.format(env.pg_version)):
        sudo('envdir /etc/wal-e.d/env wal-e backup-push /var/lib/postgresql/{}/main/'.format(env.pg_version), user='postgres')


def get_pg_version():
    return sudo('pg_dump -V')


def full_deploy():
    preparing_for_deploy()
    init_common_server_env()
    install_postgres(allow_remote_connections=True, logging_slow_queries=True)
    install_postgis()
    create_txproject_db_user()
    install_wal_e()
    configure_firewall()
    load_all_templates()
    wal_e_backup_push()
